const requestJson = require('request-json');

exports.handler = function(event,context,callback){
    const user = event.requestContext.authorizer;
    const apikey = event.stageVariables.mLabAPIKey;
    const db = event.stageVariables.baseMLabURL;
    console.log(event)
    var query = "user?"
     if (event.pathParameters && event.pathParameters.userid){
         if(user._id != event.pathParameters.userid && user.tipo == 0){
            var response = { 
                "statusCode": 401,
                "body": "{\"msg\":\"Solo puedes acceder a tus datos. Tu usuario es "+ user._id + "\"}",
                "isBase64Encoded": false
            };
            return callback(null,response);
         }
        query = query + 'q={"_id" : ' + JSON.stringify(event.pathParameters.userid) + '}'  + "&" + apikey
     } else if (event.queryStringParameters){
        query = "user?c=flase";
        if (event.queryStringParameters.id){ query = query + '&q={"_id" : ' + JSON.stringify(event.queryStringParameters.id) + '}'  }
        if (event.queryStringParameters.email){ query = query + '&q={"email" : ' + JSON.stringify(event.queryStringParameters.email) + '}'}
        if (event.queryStringParameters.limit){ query = query + '&l=' + event.queryStringParameters.limit}
        if (event.queryStringParameters.offset){ query = query + '&sk=' + event.queryStringParameters.offset}
        if (event.queryStringParameters.sort){ query = query + '&s={' + JSON.stringify(event.queryStringParameters.sort) + ': 1}'   }
        query = query + "&" + apikey
     } else {
        query = query + apikey    
    }
    console.log(query)
    
    var httpClient = requestJson.createClient(db);
    httpClient.get(query,
        function(err,resMLab,body){
            if(err){
                var response = {
                    "statusCode" : 500,
                    "body": JSON.stringify(err),
                }
            } else {
                if(body.length == 1){ 
                    body = body[0]}
                var response = { 
                    "statusCode": 200,
                    "headers" : {
                        "Access-Control-Allow-Origin": "*"
                    },
                    "body": JSON.stringify(body),
                    "isBase64Encoded": false
                    };
                }
            callback(null,response); 
        })
};

